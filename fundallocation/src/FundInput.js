import React, { useState } from 'react'
import FundNode from './FundNode'
import './FundInput.css'


function FundInput() {
    const[amount,setAmount]=useState(100);//amount to be divided
    const[tree,setTree]=useState({ //each node's initial percentage
        sales:25,
        marketingAndSales:50,
        marketing:25,
        finance:25,
        inhouseExpenses:15,
        taxAudit:10,
        operationAndMaintenance:25,
        operation:13,
        maintenance:13,
        digitalMarketing:25,
        internalOperation:13
   });
   const [sibilings, setSibilings] = useState(["marketingAndSales","finance","operationAndMaintenance"])//high level sibling
   
   const[struct,setStruct]=useState([//relations between nodes
       {
           parent:"marketingAndSales",
           child:"marketing"
       },{
           parent:"marketingAndSales",
           child:"sales"
       },{
           parent:"finance",
           child:"taxAudit"
       },{
           parent:"finance",
           child:"inhouseExpenses"
       },{
           parent:"operationAndMaintenance",
           child:"maintenance"
       },{
        parent:"operationAndMaintenance",
        child:"operation"
       },{
           parent:"marketing",
           child:"digitalMarketing"
       },{
        parent:"operation",
        child:"internalOperation"
    }
   ])
     
   const handleChange=(temp)=>{
    setTree(temp);
  
   }
   
    
    const setTreeStruct=(e,level,propName)=>{
        let updatedTree={...tree}
        if(level===1){  // if percent is changed at parent level
            //propagate it to  children
        let children=struct.filter((relation)=>{return relation.parent===propName})
        children.map((childs)=>{
            let childValue=(e.target.value/children.length);
            updatedTree[childs.child]=childValue
            let grandChildren=struct.filter((relation)=>{return relation.parent===childs.child})
            grandChildren.map((grandchilds)=>{
                                    updatedTree[grandchilds.child]=(childValue/grandChildren.length)
                                })
                        })
        updatedTree[propName]=Number(e.target.value)
        //propagate it to siblings
        let sibilingNodes=sibilings.filter((sibling)=>{return sibling!==propName})
        let siblingNodeValue=(100-Number(e.target.value))/sibilingNodes.length
        sibilingNodes.map((siblingNode)=>{
            //propagate it to siblings children
            let siblingChildren=struct.filter((relation)=>{return relation.parent===siblingNode})
            siblingChildren.map((childs)=>{
                let siblingChildValue=(siblingNodeValue/siblingChildren.length)
                updatedTree[childs.child]=siblingChildValue
                let siblingGrandChildren=struct.filter((relation)=>{return relation.parent===childs.child})
                siblingGrandChildren.map((grandchilds)=>{updatedTree[grandchilds.child]=siblingChildValue/siblingGrandChildren.length})
            })
        updatedTree[siblingNode]=Number(siblingNodeValue)
        //propagate it to siblings grandchildren

        
        })
        
        
        
        }else if(level===2){// if percent is changed at child level
            
        let root=struct.filter((relation)=>{return relation.child===propName})
        //propagate change to parent
        updatedTree[root[0].parent]=tree[root[0].parent]-tree[propName]+Number(e.target.value)
        updatedTree[propName]=Number(e.target.value)
        //propagate change to child
        let children=struct.filter((relation)=>{return relation.parent===propName})
        children.map((childs)=>{
            updatedTree[childs.child]=Number(e.target.value)/children.length
        })
        //propagate change to parent siblings
        let sibilingNodes=sibilings.filter((sibling)=>{return sibling!==root[0].parent})
        let siblingNodeValue=(100-updatedTree[root[0].parent])/sibilingNodes.length
        sibilingNodes.map((siblingNode)=>{
            
            //propagate change to parent siblings children
            let siblingChildren=struct.filter((relation)=>{return relation.parent===siblingNode})
            siblingChildren.map((childs)=>{
                let siblingChildValue=(siblingNodeValue/siblingChildren.length)
                updatedTree[childs.child]=siblingChildValue
                let siblingGrandChildren=struct.filter((relation)=>{return relation.parent===childs.child})
                siblingGrandChildren.map((grandchilds)=>{updatedTree[grandchilds.child]=siblingChildValue/siblingGrandChildren.length})
            
                })

            updatedTree[siblingNode]=Number(siblingNodeValue)
            })
        }
        else{
            updatedTree[propName]=Number(e.target.value)
            let parentNode=struct.filter((relation)=>{return relation.child===propName})
            let grandParentNode=struct.filter((relation)=>{return relation.child===parentNode[0].parent})
            //propagate it to parent
            let parentValue=tree[parentNode[0].parent]-tree[propName]+Number(e.target.value)
            updatedTree[parentNode[0].parent]=parentValue
            //propagate it to grandParent
            let grandParentValue=tree[grandParentNode[0].parent]-tree[parentNode[0].parent]+Number(e.target.value)
            updatedTree[grandParentNode[0].parent]=grandParentValue
            //propagate to grandparent siblings
            let sibilingNodes=sibilings.filter((sibling)=>{return sibling!==grandParentNode[0].parent})
        let siblingNodeValue=(100-updatedTree[grandParentNode[0].parent])/sibilingNodes.length
        sibilingNodes.map((siblingNode)=>{
            //propagate change to grand parent siblings children
            let siblingChildren=struct.filter((relation)=>{return relation.parent===siblingNode})
            siblingChildren.map((childs)=>{
                let siblingChildValue=(siblingNodeValue/siblingChildren.length)
                updatedTree[childs.child]=siblingChildValue
                let siblingGrandChildren=struct.filter((relation)=>{return relation.parent===childs.child})
                siblingGrandChildren.map((grandchilds)=>{updatedTree[grandchilds.child]=siblingChildValue/siblingGrandChildren.length})
            
                })

            updatedTree[siblingNode]=Number(siblingNodeValue)
            })

        }
        //update the intial percents with the change
        handleChange(updatedTree)
        return updatedTree[propName]
        
    };
    
   
  
        return (           
            <div className= "container">
                <div className="input-container">
                <h3 className="side-header">Enter Allocated Total Amount</h3>
                <input className="amount" min="0" onChange={e => setAmount(e.target.value) } value={amount}></input>
                </div>
                {/* <FundNode name={"maintenance"} children={[]}  setTree={setTree} tree={tree}/>  */}
                <FundNode  fund={amount} name={"marketingAndSales"}  setTree={(e)=>{setTreeStruct(e,1,"marketingAndSales")}} tree={tree} struct={struct} className={"node"} actualName="Marketing&Sales"/>
                <FundNode  fund={amount} name={"marketing"} setTree={(e)=>{setTreeStruct(e,2,"marketing")}} tree={tree} struct={struct} className={"child-node"} actualName="Marketing"/>                 
                <FundNode  fund={amount} name={"digitalMarketing"} setTree={(e)=>{setTreeStruct(e,3,"digitalMarketing")}} tree={tree} struct={struct} className={"grand-child-node"} actualName="Digital Marketing"/>
                <FundNode  fund={amount} name={"sales"}  setTree={(e)=>{setTreeStruct(e,2,"sales")}} tree={tree} struct={struct} className={"child-node"} actualName="Sales"/>  
                <FundNode  fund={amount} name={"finance"} setTree={(e)=>{setTreeStruct(e,1,"finance")}} tree={tree} struct={struct} className={"node"} actualName="Finance"/>
                <FundNode  fund={amount} name={"taxAudit"} setTree={(e)=>{setTreeStruct(e,2,"taxAudit")}} tree={tree} struct={struct} className={"child-node"} actualName="Tax Audit"/>                 
                <FundNode  fund={amount} name={"inhouseExpenses"} setTree={(e)=>{setTreeStruct(e,2,"inhouseExpenses")}} tree={tree} struct={struct} className={"child-node"} actualName="Inhouse Expenses"/>
                <FundNode  fund={amount} name={"operationAndMaintenance"} setTree={(e)=>{setTreeStruct(e,2,"operationAndMaintenance")}} tree={tree} struct={struct} className={"node"} actualName="Operations"/>
                <FundNode  fund={amount} name={"operation"} setTree={(e)=>{setTreeStruct(e,2,"operation")}} tree={tree} struct={struct} className={"child-node"} actualName="Operation"/>
                <FundNode  fund={amount} name={"internalOperation"} setTree={(e)=>{setTreeStruct(e,3,"internalOperation")}} tree={tree} struct={struct} className={"grand-child-node"} actualName="InternalOperation"/>
                <FundNode  fund={amount} name={"maintenance"} setTree={(e)=>{setTreeStruct(e,2,"maintenance")}} tree={tree} struct={struct} className={"child-node"} actualName="Maintenance"/>
            </div> 

        );
    
}
export default FundInput;
