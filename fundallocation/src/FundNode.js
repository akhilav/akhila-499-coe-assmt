import React from 'react'
import './FundNode.css'




 function FundNode(props) {
    
    

   return (
            <div className="fund-node" >
               <div className={props.className}><p className="node-name"><span>&#9660;</span> {props.actualName}</p></div>
                <input className="node-input" min="0" max="100"value={props.tree[props.name]} onChange={props.setTree}></input>
                <p className="node-value">{props.fund*props.tree[props.name]/100}</p>    
            </div>
      
          );
}
export default FundNode;
