import React from 'react';
import './App.css';
import Header from './Header'
import FundInput from './FundInput'

function App() {
 
  return (
    <div className="App">
    <Header/>
    <FundInput/> 
    
    </div>
  );
}

export default App;
